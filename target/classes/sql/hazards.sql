INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    1,
    1,
    now(),
    'There''s a puddle in the toilets',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    2,
    1,
    now(),
    'Broken toaster in the kitchen tripping the fuse box',
    'MEDIUM')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    3,
    3,
    now(),
    'The person sitting next to me smells bad',
    'LOW')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    4,
    3,
    now(),
    'I can''t find anything on my desk',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    5,
    5,
    now(),
    'My workstation is poorly adjusted',
    'MEDIUM')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    6,
    5,
    now(),
    'There is smoke coming through the air vents outside',
    'LOW')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    7,
    7,
    now(),
    'I don''t know what I''m doing with my life and it''s getting me down',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    8,
    7,
    now(),
    'I wish I hadn''t decided to create so much sample data',
    'MEDIUM')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    9,
    9,
    now(),
    'The foosball table has a rickety leg',
    'LOW')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    10,
    9,
    now(),
    'The tea bags in the office don''t meet my high standards',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    11,
    11,
    now(),
    'The radiation from my monitor is giving me sunburn',
    'MEDIUM')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    12,
    11,
    now(),
    'John wasn''t carry the scissors safely while walking through the office',
    'LOW')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    13,
    13,
    now(),
    'There is a large animal growling at me from the carpark bushes',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    14,
    13,
    now(),
    'James told me off for carrying the scissors incorrectly',
    'MEDIUM')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    15,
    15,
    now(),
    'I''m being forced to work 18 hours days by my terrible manager',
    'LOW')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    16,
    15,
    now(),
    'Everyone around me is sleep deprived and cranky and it''s making me feel postal',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    17,
    17,
    now(),
    'These are supposed to be hazards, not grips - come on people!',
    'MEDIUM')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    18,
    17,
    now(),
    'I don''t like stuff...',
    'LOW')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    19,
    19,
    now(),
    'There are a large number of cardboard boxes in the development office and noone knows how to dispose of them',
    'HIGH')
;

INSERT INTO hazards (id, reported_by, reported_on, description, severity)
VALUES
  (
    20,
    19,
    now(),
    'There is a cable laying across the floor of the common area acting as a trip hazard',
    'MEDIUM')
;
