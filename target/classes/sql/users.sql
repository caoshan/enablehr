INSERT INTO users (id, username, password)
VALUES
  (
    1,
    'jane.doe',
    'jane.doe')
;

INSERT INTO users (id, username, password)
VALUES
  (
    2,
    'john.doe',
    '`john.doe')
;

INSERT INTO users (id, username, password)
VALUES
  (
    3,
    'jim.doe',
    'jim.doe')
;

INSERT INTO users (id, username, password)
VALUES
  (
    4,
    'james.doe',
    'james.doe')
;

INSERT INTO users (id, username, password)
VALUES
  (
    5,
    'jane.smith',
    'jane.smith')
;

INSERT INTO users (id, username, password)
VALUES
  (
    6,
    'john.smith',
    'john.smith')
;

INSERT INTO users (id, username, password)
VALUES
  (
    7,
    'jim.smith',
    'jim.smith')
;

INSERT INTO users (id, username, password)
VALUES
  (
    8,
    'james.smith',
    'james.smith')
;

INSERT INTO users (id, username, password)
VALUES
  (
    9,
    'jane.jones',
    'jane.jones')
;

INSERT INTO users (id, username, password)
VALUES
  (
    10,
    'john.jones',
    'john.jones')
;

INSERT INTO users (id, username, password)
VALUES
  (
    11,
    'jim.jones',
    'jim.jones')
;

INSERT INTO users (id, username, password)
VALUES
  (
    12,
    'james.jones',
    'james.jones')
;

INSERT INTO users (id, username, password)
VALUES
  (
    13,
    'jane.jackson',
    'jane.jackson')
;

INSERT INTO users (id, username, password)
VALUES
  (
    14,
    'john.jackson',
    'john.jackson')
;

INSERT INTO users (id, username, password)
VALUES
  (
    15,
    'jim.jackson',
    'jim.jackson')
;

INSERT INTO users (id, username, password)
VALUES
  (
    16,
    'james.jackson',
    'james.jackson')
;

INSERT INTO users (id, username, password)
VALUES
  (
    17,
    'jane.james',
    'jane.james')
;

INSERT INTO users (id, username, password)
VALUES
  (
    18,
    'john.james',
    'john.james')
;

INSERT INTO users (id, username, password)
VALUES
  (
    19,
    'jim.james',
    'jim.james')
;

INSERT INTO users (id, username, password)
VALUES
  (
    20,
    'james.james',
    'james.james')
;

