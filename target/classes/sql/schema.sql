CREATE TABLE users (
  id       IDENTITY PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255),
  UNIQUE (username)
)
;

CREATE TABLE user_authority_names (
  user_id   INTEGER      NOT NULL,
  authority VARCHAR(255) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id),
  UNIQUE (user_id, authority)
)
;

CREATE TABLE personal_details (
  id          IDENTITY PRIMARY KEY,
  user_id     INTEGER      NOT NULL,
  email       VARCHAR(255) NOT NULL,
  family_name VARCHAR(255) NOT NULL,
  given_name  VARCHAR(255) NOT NULL,
  street      VARCHAR(255),
  suburb      VARCHAR(255),
  state       VARCHAR(255),
  post_code   VARCHAR(255),
  country     VARCHAR(255),
  FOREIGN KEY (user_id) REFERENCES users (id),
  UNIQUE (user_id),
  UNIQUE (email)
)
;

CREATE TABLE hazards (
  id          IDENTITY PRIMARY KEY,
  reported_by INTEGER      NOT NULL,
  reported_on TIMESTAMP    NOT NULL,
  description CLOB         NOT NULL,
  severity    VARCHAR(255) NOT NULL,
  FOREIGN KEY (reported_by) REFERENCES users (id)
)
;

CREATE TABLE hazards_incident (
  id          IDENTITY PRIMARY KEY,
  reported_by INTEGER      NOT NULL,
  reported_on TIMESTAMP    NOT NULL,
  description CLOB         NOT NULL,
  severity    VARCHAR(255) NOT NULL,
  hazard_id   INTEGER      NOT NULL,
  status      VARCHAR(255) NOT NULL,
  FOREIGN KEY (reported_by) REFERENCES users (id),
  FOREIGN KEY (hazard_id) REFERENCES hazards (id)
)
;

CREATE TABLE personal_details_incident (
  id          IDENTITY PRIMARY KEY,
  email       VARCHAR(255) NOT NULL,
  family_name VARCHAR(255) NOT NULL,
  given_name  VARCHAR(255) NOT NULL,
  street      VARCHAR(255),
  suburb      VARCHAR(255),
  state       VARCHAR(255),
  post_code   VARCHAR(255),
  country     VARCHAR(255),
  pd_id       INTEGER      NOT NULL,
  status      VARCHAR(255) NOT NULL,
  FOREIGN KEY (pd_id) REFERENCES personal_details (id)
)
;
