Feature: Update personal details

  As a user
  I want to update my personal details
  So that I don't need to rely on my manager to do it for me

  Scenario: User updates their own details
    Given I am logged in as:
      | Username | jane.smith |
      | Password | jane.smith |
    When I go to the "/personal-details" page
    And I enter the following details:
      | Email       | janet@example.com |
      | Given name  | Janet             |
      | Family name | Smith             |
      | Street      | 42 Wallaby Way    |
      | Suburb      | Sydney            |
      | Post code   | 2001              |
      | State       | NSW               |
      | Country     | Australia         |
    And I click the "save" element
    Then I should see the message "Your details have been saved"
    And I should see the following fields:
      | Email       | janet@example.com |
      | Given name  | Janet             |
      | Family name | Smith             |
      | Street      | 42 Wallaby Way    |
      | Suburb      | Sydney            |
      | Post code   | 2001              |
      | State       | NSW               |
      | Country     | Australia         |
