package au.com.enablehr.homework.feature.config;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.concurrent.TimeUnit.SECONDS;

@Configuration
public class WebDriverConfiguration {

    @Bean(destroyMethod = "quit")
    public FirefoxDriver firefoxDriver() {

        final FirefoxDriver firefoxDriver = new FirefoxDriver();

        firefoxDriver.manage()
                     .timeouts()
                     .implicitlyWait(1, SECONDS)
                     .pageLoadTimeout(5, SECONDS)
                     .setScriptTimeout(5, SECONDS);


        return firefoxDriver;
    }

}
