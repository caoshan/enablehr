package au.com.enablehr.homework.feature.hook;

import au.com.enablehr.homework.feature.config.WebDriverConfiguration;
import cucumber.api.java.Before;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = WebDriverConfiguration.class)
public class SetScreenSizeHook {

    private Dimension dimension = new Dimension(800, 600);

    @Autowired
    private WebDriver webDriver;

    @Before(order = 25_000)
    public void beforeScenario() {

        webDriver.manage()
                 .window()
                 .setSize(dimension);
    }

}
