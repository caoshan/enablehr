package au.com.enablehr.homework.feature.hook;

import au.com.enablehr.homework.feature.config.WebDriverConfiguration;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = WebDriverConfiguration.class)
public class ClearCookiesHook {

    @Autowired
    private WebDriver webDriver;

    @After(order = 15_000)
    @Before(order = 15_000)
    public void clearCookies() {

        webDriver.manage()
                 .deleteAllCookies();
    }

}
