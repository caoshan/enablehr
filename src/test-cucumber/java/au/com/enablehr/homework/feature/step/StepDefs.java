package au.com.enablehr.homework.feature.step;

import au.com.enablehr.homework.feature.config.WebDriverConfiguration;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.table.CamelCaseStringConverter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.net.URL;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.tagName;

@ContextConfiguration(classes = WebDriverConfiguration.class)
public class StepDefs {

    private static final String BASE_URL = "http://localhost:8080/homework";

    @Autowired
    private WebDriver webDriver;

    @Given("^a user$")
    public void aUser() throws Throwable {

        assertNotNull("a user should have a web browser", webDriver);
    }

    @Given("^I am logged in as:$")
    public void iAmLoggedInAs(final Map<String, String> credentials) throws Throwable {

        iGoToThePage("/login");
        iEnterTheFollowingDetails(credentials);
        iClickTheElement("login");
    }

    @Given("^I go to the \"(.*?)\" page$")
    public void iGoToThePage(final String path) throws Throwable {

        webDriver.navigate()
                 .to(new URL(BASE_URL + path));
    }

    @And("^I click the \"([^\"]*)\" element$")
    public void iClickTheElement(final String element) throws Throwable {

        webDriver.findElement(id(element))
                 .click();
    }

    @When("^I enter the following details:$")
    public void iEnterTheFollowingDetails(final Map<String, String> details) throws Throwable {

        final CamelCaseStringConverter camelCaseStringConverter = new CamelCaseStringConverter();
        for (final Map.Entry<String, String> entry : details.entrySet()) {
            final String name = entry.getKey();
            final String value = entry.getValue();
            final String id = camelCaseStringConverter.map(name);

            final WebElement element = webDriver.findElement(id(id));


            final String tagName = element.getTagName();
            switch (tagName) {
                case "select":
                    new Select(element).selectByValue(value);
                    break;
                default:
                    element.clear();
                    element.sendKeys(value);
            }
        }
    }

    @Then("^I should not see the links:$")
    public void iShouldNotSeeTheLinks(final Map<String, String> links) throws Throwable {

        final List<WebElement> elements = webDriver.findElements(tagName("a"));
        for (final Map.Entry<String, String> entry : links.entrySet()) {
            final String text = entry.getKey();
            final String href = entry.getValue();
            boolean found = false;
            for (final WebElement element : elements) {
                if (element.getText()
                           .contains(text)
                    && element.getAttribute("href")
                              .endsWith(href)) {
                    found = true;
                    break;
                }
            }
            assertFalse(format("A link with href='%s' and text='%s' should not be found", href, text), found);
        }
    }

    @Then("^I should see the following fields:$")
    public void iShouldSeeTheFollowingFields(final Map<String, String> fields) throws Throwable {

        final CamelCaseStringConverter camelCaseStringConverter = new CamelCaseStringConverter();
        for (final Map.Entry<String, String> entry : fields.entrySet()) {
            final String name = entry.getKey();
            final String value = entry.getValue();
            final String id = camelCaseStringConverter.map(name);

            final WebElement element = webDriver.findElement(id(id));

            assertThat(format("element '%s' should contain the text '%s'", name, value),
                       element.getAttribute("value"),
                       containsString(value));
        }
    }

    @Then("^I should see the links:$")
    public void iShouldSeeTheLinks(final Map<String, String> links) throws Throwable {

        final List<WebElement> elements = webDriver.findElements(tagName("a"));
        for (final Map.Entry<String, String> entry : links.entrySet()) {
            final String text = entry.getKey();
            final String href = entry.getValue();
            boolean found = false;
            for (final WebElement element : elements) {
                if (element.getText()
                           .contains(text)
                    && element.getAttribute("href")
                              .endsWith(href)) {
                    found = true;
                    break;
                }
            }
            assertTrue(format("A link with href='%s' and text='%s' should be found", href, text), found);
        }
    }

    @Then("^I should see the message \"([^\"]*)\"$")
    public void iShouldSeeTheMessage(final String message) throws Throwable {

        final WebElement element = webDriver.findElement(id("message"));

        assertThat(format("message should contain the text '%s'", message), element.getText(), containsString(message));
    }

    @Then("^I should see the previously reported hazards:$")
    public void iShouldSeeThePreviouslyReportedHazards(final List<Map<String, String>> hazards) throws Throwable {

        for (final Map<String, String> hazard : hazards) {

            final WebElement table = hazard.containsKey("Reported by") ? webDriver.findElement(id("otherHazards"))
                                                                       : webDriver.findElement(id("myHazards"));

            final WebElement tbody = table.findElement(tagName("tbody"));

            for (final String text : hazard.values()) {
                assertThat(format("row should contain the text '%s'", text), tbody.getText(), containsString(text));
            }
        }
    }

    @Then("^I should see the text \"([^\"]*)\"$")
    public void iShouldSeeTheText(final String text) throws Throwable {

        final WebElement element = webDriver.findElement(tagName("body"));

        assertThat(format("page should contain the text '%s'", text), element.getText(), containsString(text));
    }

}
