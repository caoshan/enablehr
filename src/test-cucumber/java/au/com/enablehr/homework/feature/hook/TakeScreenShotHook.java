package au.com.enablehr.homework.feature.hook;

import au.com.enablehr.homework.feature.config.WebDriverConfiguration;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.openqa.selenium.OutputType.BYTES;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

@ContextConfiguration(classes = WebDriverConfiguration.class)
public class TakeScreenShotHook {

    @Autowired
    private TakesScreenshot takesScreenshot;

    @After(order = 20_000)
    public void afterScenario(final Scenario scenario) {

        scenario.embed(takesScreenshot.getScreenshotAs(BYTES), IMAGE_PNG_VALUE);
    }

}
