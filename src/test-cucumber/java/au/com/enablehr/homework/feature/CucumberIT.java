package au.com.enablehr.homework.feature;

import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@Cucumber.Options(format = {"html:target/cucumber",
                            "progress"},
                  glue = {"au.com.enablehr.homework.feature.hook",
                          "au.com.enablehr.homework.feature.step"},
                  snippets = SnippetType.CAMELCASE)
public class CucumberIT {

    // Nothing to see here

}
