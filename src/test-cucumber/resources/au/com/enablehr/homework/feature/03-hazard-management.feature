Feature: Hazard management

  As an employee
  I want to report hazards
  So that they can be properly managed for my safety

  Scenario: Manager views all staff hazards
    Given I am logged in as:
      | Username | jim.jones |
      | Password | jim.jones |
    And I go to the "/list-hazards" page
    Then I should see the previously reported hazards:
      | Description                                                            | Severity |
      | The radiation from my monitor is giving me sunburn                     | MEDIUM   |
      | John wasn't carry the scissors safely while walking through the office | LOW      |
    And I should see the previously reported hazards:
      | Description                                                   | Severity | Reported by |
      | There's a puddle in the toilets                               | HIGH     | jane.doe    |
      | My workstation is poorly adjusted                             | MEDIUM   | jane.smith  |
      | I'm being forced to work 18 hours days by my terrible manager | LOW      | jim.jackson |

  Scenario: Users views their own reported hazards
    Given I am logged in as:
      | Username | jim.doe |
      | Password | jim.doe |
    And I go to the "/list-hazards" page
    Then I should see the previously reported hazards:
      | Description                              | Severity |
      | The person sitting next to me smells bad | LOW      |
      | I can't find anything on my desk         | HIGH     |

  Scenario: Users submits a new hazard
    Given I am logged in as:
      | Username | jim.james |
      | Password | jim.james |
    And I go to the "/report-hazard" page
    And I enter the following details:
      | Severity    | HIGH                                      |
      | Description | Writing cucumber tests is getting me down |
    And I click the "save" element
    Then I should see the previously reported hazards:
      | Description                                                                                                  | Severity |
      | There are a large number of cardboard boxes in the development office and noone knows how to dispose of them | HIGH     |
      | There is a cable laying across the floor of the common area acting as a trip hazard                          | MEDIUM     |
      | Writing cucumber tests is getting me down                                                                    | HIGH     |
