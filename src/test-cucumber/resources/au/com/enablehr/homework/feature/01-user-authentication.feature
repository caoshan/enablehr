Feature: User authentication

  As a user
  In order to access and update my information
  I want to log in with my own credentials

  Scenario: Manager login
    Given a user
    When I go to the "/login" page
    And I enter the following details:
      | Username | jane.doe |
      | Password | jane.doe |
    And I click the "login" element
    Then I should see the text "Welcome back jane.doe"
    And I should see the links:
      | Personal details | /personal-details |
      | Employees        | /list-employees   |
      | Hazards          | /list-hazards     |
      # Logout isn't a link - this step doesn't know how to handle forms
      # | Log out          | /logout           |

  Scenario: Employee login
    Given a user
    When I go to the "/login" page
    And I enter the following details:
      | Username | jim.doe |
      | Password | jim.doe |
    And I click the "login" element
    Then I should see the text "Welcome back jim.doe"
    And I should see the links:
      | Personal details | /personal-details |
      | Hazards          | /list-hazards     |
      # Logout isn't a link - this step doesn't know how to handle forms
      # | Log out          | /logout           |
    And I should not see the links:
      | Employees | /list-employees |
