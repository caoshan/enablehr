package au.com.enablehr.homework.domain;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserTest {

    private User subject;

    @Before
    public void setUp() throws Exception {

        subject = new User();
    }

    @Test
    public void testGetAuthorities() throws Exception {

        final Collection<String> authorityNames = new ArrayList<>();
        authorityNames.add("authority 1");
        authorityNames.add("authority 2");
        subject.setAuthorityNames(authorityNames);

        final Collection<? extends GrantedAuthority> result = subject.getAuthorities();

        final Iterator<? extends GrantedAuthority> iterator = result.iterator();
        assertEquals("getAuthorities should return authorities based on authority names",
                     iterator.next(),
                     new SimpleGrantedAuthority("authority 1"));
        assertEquals("getAuthorities should return authorities based on authority names",
                     iterator.next(),
                     new SimpleGrantedAuthority("authority 2"));
    }

    @Test
    public void testGetAuthorityNames() throws Exception {

        final Collection<String> authorityNames = new ArrayList<>();
        authorityNames.add("authority 1");
        authorityNames.add("authority 2");
        subject.setAuthorityNames(authorityNames);

        final Collection<String> result = subject.getAuthorityNames();

        assertEquals("getAuthorityNames()/setAuthorityNames() should be consistent", authorityNames, result);
    }

    @Test
    public void testGetId() throws Exception {

        subject.setId(1234);

        final Integer result = subject.getId();

        assertEquals("getId()/setId() should be consistent", Integer.valueOf(1234), result);
    }

    @Test
    public void testGetPassword() throws Exception {

        subject.setPassword("password");

        final String result = subject.getPassword();

        assertEquals("getPassword()/setPassword() should be consistent", "password", result);
    }

    @Test
    public void testGetPersonalDetails() throws Exception {

        final PersonalDetails personalDetails = new PersonalDetails();
        subject.setPersonalDetails(personalDetails);

        final PersonalDetails result = subject.getPersonalDetails();

        assertEquals("getPersonalDetails()/setPersonalDetails() should be consistent", personalDetails, result);
    }

    @Test
    public void testGetUsername() throws Exception {

        subject.setUsername("username");

        final String result = subject.getUsername();

        assertEquals("getUsername()/setUsername() should be consistent", "username", result);
    }

    @Test
    public void testIsAccountNonExpired() throws Exception {

        final boolean result = subject.isAccountNonExpired();

        assertTrue("isAccountNonExpired() should always be true", result);
    }

    @Test
    public void testIsAccountNonLocked() throws Exception {

        final boolean result = subject.isAccountNonLocked();

        assertTrue("isAccountNonLocked() should always be true", result);
    }

    @Test
    public void testIsCredentialsNonExpired() throws Exception {

        final boolean result = subject.isCredentialsNonExpired();

        assertTrue("isCredentialsNonExpired() should always be true", result);
    }

    @Test
    public void testIsEnabled() throws Exception {

        final boolean result = subject.isEnabled();

        assertTrue("isEnabled() should always be true", result);
    }

}
