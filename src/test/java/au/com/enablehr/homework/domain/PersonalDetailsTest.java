package au.com.enablehr.homework.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonalDetailsTest {

    private PersonalDetails subject;

    @Before
    public void setUp() throws Exception {

        subject = new PersonalDetails();
    }

    @Test
    public void testGetAddress() throws Exception {

        final Address address = new Address();
        subject.setAddress(address);

        final Address result = subject.getAddress();

        assertEquals("getAddress()/setAddress() should be consistent", address, result);
    }

    @Test
    public void testGetEmail() throws Exception {

        subject.setEmail("email@example.com");

        final String result = subject.getEmail();

        assertEquals("getEmail()/setEmail() should be consistent", "email@example.com", result);
    }

    @Test
    public void testGetFamilyName() throws Exception {

        subject.setFamilyName("family name");

        final String result = subject.getFamilyName();

        assertEquals("getFamilyName()/setFamilyName() should be consistent", "family name", result);
    }

    @Test
    public void testGetGivenName() throws Exception {

        subject.setGivenName("given name");

        final String result = subject.getGivenName();

        assertEquals("getGivenName()/setGivenName() should be consistent", "given name", result);
    }

    @Test
    public void testGetId() throws Exception {

        subject.setId(1234);

        final Integer result = subject.getId();

        assertEquals("getId()/setId() should be consistent", Integer.valueOf(1234), result);
    }

    @Test
    public void testGetUser() throws Exception {

        final User user = new User();
        subject.setUser(user);

        final User result = subject.getUser();

        assertEquals("getUser()/setUser() should be consistent", user, result);
    }

}
