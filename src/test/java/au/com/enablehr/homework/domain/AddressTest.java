package au.com.enablehr.homework.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddressTest {

    private Address subject;

    @Before
    public void setUp() throws Exception {

        subject = new Address();
    }

    @Test
    public void testGetCountry() throws Exception {

        subject.setCountry("country");

        final String result = subject.getCountry();

        assertEquals("getCountry()/setCountry() should be consistent", "country", result);
    }

    @Test
    public void testGetPostCode() throws Exception {

        subject.setPostCode("post code");

        final String result = subject.getPostCode();

        assertEquals("getPostCode()/setPostCode() should be consistent", "post code", result);
    }

    @Test
    public void testGetState() throws Exception {

        subject.setState("state");

        final String result = subject.getState();

        assertEquals("getState()/setState() should be consistent", "state", result);
    }

    @Test
    public void testGetStreet() throws Exception {

        subject.setStreet("street");

        final String result = subject.getStreet();

        assertEquals("getStreet()/setStreet() should be consistent", "street", result);
    }

    @Test
    public void testGetSuburb() throws Exception {

        subject.setSuburb("suburb");

        final String result = subject.getSuburb();

        assertEquals("getSuburb()/setSuburb() should be consistent", "suburb", result);
    }

}
