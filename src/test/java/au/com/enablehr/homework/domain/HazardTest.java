package au.com.enablehr.homework.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class HazardTest {

    private Hazard subject;

    @Before
    public void setUp() throws Exception {

        subject = new Hazard();
    }

    @Test
    public void testGetDescription() throws Exception {

        subject.setDescription("description");

        final String result = subject.getDescription();

        assertEquals("getDescription()/setDescription() should be consistent", "description", result);
    }

    @Test
    public void testGetId() throws Exception {

        subject.setId(1234);

        final Integer result = subject.getId();

        assertEquals("getId()/setId() should be consistent", Integer.valueOf(1234), result);
    }

    @Test
    public void testGetReportedBy() throws Exception {

        final User user = new User();
        subject.setReportedBy(user);

        final User result = subject.getReportedBy();

        assertEquals("getReportedBy()/setReportedBy() should be consistent", user, result);
    }

    @Test
    public void testGetReportedOn() throws Exception {

        final Date date = new Date();
        subject.setReportedOn(date);

        final Date result = subject.getReportedOn();

        assertEquals("getReportedOn()/setReportedOn() should be consistent", date, result);
    }

    @Test
    public void testGetSeverity() throws Exception {

        subject.setSeverity(Hazard.Severity.MEDIUM);

        final Hazard.Severity result = subject.getSeverity();

        assertEquals("getSeverity()/setSeverity() should be consistent", Hazard.Severity.MEDIUM, result);
    }

}
