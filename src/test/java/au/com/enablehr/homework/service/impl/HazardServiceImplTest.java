package au.com.enablehr.homework.service.impl;

import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.repository.HazardIncidentRepository;
import au.com.enablehr.homework.repository.HazardRepository;
import au.com.enablehr.homework.service.UserService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HazardServiceImplTest {

    @Mock
    private HazardRepository hazardRepository;
    
    @Mock
    private HazardIncidentRepository hazardIncidentRepository;

    private HazardServiceImpl subject;

    @Mock
    private UserService userService;

    @Before
    public void setUp() throws Exception {

        subject = new HazardServiceImpl(hazardRepository, userService, hazardIncidentRepository);
    }

    @Test
    public void testGetAll() throws Exception {

        final Hazard hazard1 = new Hazard();
        final Hazard hazard2 = new Hazard();
        when(hazardRepository.findAll()).thenReturn(asList(hazard1, hazard2));

        final Iterable<Hazard> result = subject.getAll();

        assertThat("getAll() should return all hazards", result, hasItems(hazard1, hazard2));
    }

    @Test
    public void testGetAllForCurrentUser() throws Exception {

        final User user = new User();
        when(userService.getCurrentUser()).thenReturn(user);
        final Hazard hazard1 = new Hazard();
        final Hazard hazard2 = new Hazard();
        when(hazardRepository.findAllReportedBy(any(User.class))).thenReturn(asList(hazard1, hazard2));

        final List<Hazard> result = subject.getAllForCurrentUser();

        assertThat("getAllForCurrentUser() should return all hazards for the current user", result, hasItems(hazard1,
                                                                                                             hazard2));
    }

    @Test
    public void testSave() throws Exception {

        final Hazard hazard = new Hazard();

        subject.save(hazard);

        verify(hazardRepository).save(hazard);
    }

}
