package au.com.enablehr.homework.service.impl;

import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    private UserServiceImpl subject;

    @Mock
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {

        subject = new UserServiceImpl(userRepository);
    }

    @Test
    public void testGetCurrentUser() throws Exception {

        final User user = new User();
        SecurityContextHolder.getContext()
                             .setAuthentication(new UsernamePasswordAuthenticationToken(user, null));

        final User result = subject.getCurrentUser();

        assertEquals("getCurrentUser() should return the current user", result, user);
    }

    @Test
    public void testSave() throws Exception {

        final User user = new User();

        subject.save(user);

        verify(userRepository).save(user);
    }

}
