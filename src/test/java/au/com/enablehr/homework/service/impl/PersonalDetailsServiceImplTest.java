package au.com.enablehr.homework.service.impl;

import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.repository.PersonalDetailsIncidentRepository;
import au.com.enablehr.homework.repository.PersonalDetailsRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonalDetailsServiceImplTest {

    @Mock
    private PersonalDetailsRepository personalDetailsRepository;
    
    @Mock
    private PersonalDetailsIncidentRepository personalDetailsIncidentRepository;

    private PersonalDetailsServiceImpl subject;

    @Before
    public void setUp() throws Exception {

        subject = new PersonalDetailsServiceImpl(personalDetailsRepository, personalDetailsIncidentRepository);
    }

    @Test
    public void testGetAll() throws Exception {

        final PersonalDetails personalDetails1 = new PersonalDetails();
        final PersonalDetails personalDetails2 = new PersonalDetails();
        when(personalDetailsRepository.findAll()).thenReturn(asList(personalDetails1, personalDetails2));

        final Iterable<PersonalDetails> result = subject.getAll();

        assertThat("getAll() should return all personal details", result, hasItems(personalDetails1, personalDetails2));
    }

}
