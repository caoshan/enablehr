package au.com.enablehr.homework.repository;

import au.com.enablehr.homework.config.JpaConfig;
import au.com.enablehr.homework.config.SecurityConfig;
import au.com.enablehr.homework.config.ServiceConfig;
import au.com.enablehr.homework.config.TilesConfig;
import au.com.enablehr.homework.config.WebMvcConfig;
import au.com.enablehr.homework.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@ContextHierarchy({@ContextConfiguration(classes = {JpaConfig.class,
                                                    SecurityConfig.class,
                                                    ServiceConfig.class,
                                                    WebMvcConfig.class}),
                   @ContextConfiguration(classes = TilesConfig.class)})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class UserRepositoryIT {

    @Autowired
    private UserRepository subject;

    @Test
    public void testFindByUsername() throws Exception {

        final User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        subject.save(user);

        final User result = subject.findByUsername("username");

        assertEquals("findByUsername() should return the requested username", user, result);
    }

}
