package au.com.enablehr.homework.repository;

import au.com.enablehr.homework.config.JpaConfig;
import au.com.enablehr.homework.config.SecurityConfig;
import au.com.enablehr.homework.config.ServiceConfig;
import au.com.enablehr.homework.config.TilesConfig;
import au.com.enablehr.homework.config.WebMvcConfig;
import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

@ContextHierarchy({@ContextConfiguration(classes = {JpaConfig.class,
                                                    SecurityConfig.class,
                                                    ServiceConfig.class,
                                                    WebMvcConfig.class}),
                   @ContextConfiguration(classes = TilesConfig.class)})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class HazardRepositoryIT {

    @Autowired
    private HazardRepository subject;

    private User user1;

    private User user2;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindAllReportedBy() throws Exception {

        user1 = new User();
        user1.setUsername("username 1");
        user1.setPassword("password 1");
        userRepository.save(user1);

        user2 = new User();
        user2.setUsername("username 2");
        user2.setPassword("password 2");
        userRepository.save(user2);

        for (int i = 0; i < 100; i++) {

            final Hazard hazard = new Hazard();

            hazard.setDescription(format("hazard description %03d", i));
            hazard.setReportedBy(i % 2 == 0 ? user1 : user2);
            hazard.setReportedOn(new Date(System.currentTimeMillis() - (1000L * 60L)));
            hazard.setSeverity(i % 2 == 0 ? Hazard.Severity.HIGH : Hazard.Severity.LOW);

            subject.save(hazard);
        }

        final List<Hazard> result = subject.findAllReportedBy(user1);

        assertEquals("findAllReportedBy() should return the correct number of hazards reported by a user",
                     50,
                     result.size());
        for (final Hazard hazard : result) {
            assertEquals("findAllReportedBy() should return hazards reported by the requested user",
                         user1,
                         hazard.getReportedBy());
        }
    }

}
