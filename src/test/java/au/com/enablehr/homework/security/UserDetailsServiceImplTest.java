package au.com.enablehr.homework.security;

import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

    private UserDetailsServiceImpl subject;

    @Mock
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {

        subject = new UserDetailsServiceImpl(userRepository);
    }

    @Test
    public void testLoadUserByUsername() throws Exception {

        final User user = new User();
        when(userRepository.findByUsername(anyString())).thenReturn(user);

        final UserDetails result = subject.loadUserByUsername("username");

        assertEquals("loadUserByUsername() should return the requested user", user, result);
    }

}
