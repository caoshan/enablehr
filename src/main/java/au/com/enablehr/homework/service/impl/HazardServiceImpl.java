package au.com.enablehr.homework.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.enablehr.homework.config.WebPageConstants;
import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.domain.HazardIncident;
import au.com.enablehr.homework.domain.Status;
import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.repository.HazardIncidentRepository;
import au.com.enablehr.homework.repository.HazardRepository;
import au.com.enablehr.homework.service.HazardService;
import au.com.enablehr.homework.service.UserService;

@Service
@Transactional
public class HazardServiceImpl implements HazardService {

	// TODO configure in setting file
	private static final String DEFAULT_SORTBY = "reportedOn";

	private final HazardRepository hazardRepository;

	private final HazardIncidentRepository hazardIncidentRepository;

	private final UserService userService;

	@Autowired
	public HazardServiceImpl(final HazardRepository hazardRepository, final UserService userService,
			final HazardIncidentRepository hazardIncidentRepository) {

		this.hazardRepository = hazardRepository;
		this.userService = userService;
		this.hazardIncidentRepository = hazardIncidentRepository;
	}

	@Override
	public Iterable<Hazard> getAll() {

		return hazardRepository.findAll();
	}

	@Override
	public List<Hazard> getAllForCurrentUser() {

		final User user = userService.getCurrentUser();

		return hazardRepository.findAllReportedBy(user);
	}

	@Override
	public Hazard save(final Hazard hazard) {

		return hazardRepository.save(hazard);
	}

	@Override
	public Hazard get(Integer id) {
		Hazard hazard = hazardRepository.findOne(id);
		if (hazard != null)
			hazard.getHazardIncidents();
		return hazard;
	}

	@Override
	public Page<Hazard> findPageHazards(Integer pageNumber, String sortby) {
		if (sortby == null || "".equals(sortby))
			sortby = DEFAULT_SORTBY;
		PageRequest request = new PageRequest(pageNumber - 1, WebPageConstants.DEFAULT_PAGE_SIZE, Sort.Direction.ASC,
				sortby);
		return hazardRepository.findAll(request);
	}

	@Override
	public HazardIncident saveIncident(HazardIncident hazardIncident) {
		return hazardIncidentRepository.save(hazardIncident);
	}

	@Override
	public List<HazardIncident> getAllNewHazardIncidents() {

		return hazardIncidentRepository.getAllNewHazardIncidents();
	}

	@Override
	public Hazard approveIncdient(Integer id) {
		HazardIncident hi = hazardIncidentRepository.findOne(id);
		if (hi.getStatus() == Status.INITIAL) {
			Hazard hazard = hi.getHazard();
			hazard.setDescription(hi.getDescription());
			hazard.setSeverity(hi.getSeverity());

			hi.setStatus(Status.APPROVED);
			hazardIncidentRepository.save(hi);
			hazard = hazardRepository.save(hazard);
			return hazard;
		}
		return hi.getHazard();
	}

	@Override
	public Hazard rejectIncdient(Integer id) {
		HazardIncident hi = hazardIncidentRepository.findOne(id);
		hi.setStatus(Status.REJECTED);
		hi = hazardIncidentRepository.save(hi);
		return hi.getHazard();
	}

}
