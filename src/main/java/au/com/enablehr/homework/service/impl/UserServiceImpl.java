package au.com.enablehr.homework.service.impl;

import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.repository.UserRepository;
import au.com.enablehr.homework.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	@Autowired
	public UserServiceImpl(final UserRepository userRepository) {

		this.userRepository = userRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public User getCurrentUser() {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return (User) authentication.getPrincipal();
	}

	@Override
	public void save(final User user) {

		userRepository.save(user);
	}

}
