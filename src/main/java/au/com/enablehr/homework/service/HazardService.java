package au.com.enablehr.homework.service;

import java.util.List;

import org.springframework.data.domain.Page;

import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.domain.HazardIncident;

public interface HazardService {

    Iterable<Hazard> getAll();

    List<Hazard> getAllForCurrentUser();

    Hazard save(Hazard hazard);
    
    Hazard get(Integer id);
    
    Page<Hazard> findPageHazards(Integer pageNumber, String sortby);

	HazardIncident saveIncident(HazardIncident hazardIncident);

	List<HazardIncident> getAllNewHazardIncidents();

	Hazard approveIncdient(Integer id);

	Hazard rejectIncdient(Integer id);

}
