package au.com.enablehr.homework.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import au.com.enablehr.homework.config.WebPageConstants;
import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.domain.PersonalDetailsIncident;
import au.com.enablehr.homework.domain.Status;
import au.com.enablehr.homework.repository.PersonalDetailsIncidentRepository;
import au.com.enablehr.homework.repository.PersonalDetailsRepository;
import au.com.enablehr.homework.service.PersonalDetailsService;

@Service
@Transactional
public class PersonalDetailsServiceImpl implements PersonalDetailsService {

	private static final String DEFAULT_SORTBY = "givenName";

	private PersonalDetailsRepository personalDetailsRepository;

	private PersonalDetailsIncidentRepository personalDetailsIncidentRepository;

	@Autowired
	public PersonalDetailsServiceImpl(PersonalDetailsRepository personalDetailsRepository,
			PersonalDetailsIncidentRepository personalDetailsIncidentRepository) {
		this.personalDetailsRepository = personalDetailsRepository;
		this.personalDetailsIncidentRepository = personalDetailsIncidentRepository;
	}

	@Override
	public Iterable<PersonalDetails> getAll() {

		return personalDetailsRepository.findAll();
	}

	@Override
	public PersonalDetails save(PersonalDetails personalDetails) {
		return personalDetailsRepository.save(personalDetails);
	}

	@Override
	public PersonalDetails get(Integer id) {
		return personalDetailsRepository.findOne(id);
	}

	@Override
	public Page<PersonalDetails> findPageEmployees(Integer pageNumber, String sortby) {
		if (sortby == null || "".equals(sortby))
			sortby = DEFAULT_SORTBY;
		PageRequest request = new PageRequest(pageNumber - 1, WebPageConstants.DEFAULT_PAGE_SIZE, Sort.Direction.ASC,
				sortby);
		return personalDetailsRepository.findAll(request);

	}

	@Override
	public PersonalDetailsIncident saveIncident(PersonalDetailsIncident incident) {
		return personalDetailsIncidentRepository.save(incident);
	}

	@Override
	public List<PersonalDetailsIncident> getAllNewPersonalDetailsIncidents() {
		return personalDetailsIncidentRepository.getAllNewPersonalDetailsIncidents();
	}

	@Override
	public PersonalDetails approveIncdient(Integer id) {
		PersonalDetailsIncident pi = personalDetailsIncidentRepository.findOne(id);
		if (pi.getStatus() == Status.INITIAL) {
			PersonalDetails pd = pi.getPersonalDetails();
			pd.setAddress(pi.getAddress());
			pd.setEmail(pi.getEmail());
			pd.setFamilyName(pi.getFamilyName());
			pd.setGivenName(pi.getGivenName());
			pi.setStatus(Status.APPROVED);
			personalDetailsIncidentRepository.save(pi);
			pd = personalDetailsRepository.save(pd);
			return pd;

		}
		return pi.getPersonalDetails();
	}

	@Override
	public PersonalDetails rejectIncdient(Integer id) {
		PersonalDetailsIncident pi = personalDetailsIncidentRepository.findOne(id);
		pi.setStatus(Status.REJECTED);
		pi = personalDetailsIncidentRepository.save(pi);
		return pi.getPersonalDetails();
	}

	@Override
	public List<PersonalDetailsIncident> getPersonalDetailsIncidents(PersonalDetails personalDetails) {
		return personalDetailsIncidentRepository.getIncidents(personalDetails);
	}

}
