package au.com.enablehr.homework.service;

import au.com.enablehr.homework.domain.User;

public interface UserService {

    User getCurrentUser();

    void save(User user);

}
