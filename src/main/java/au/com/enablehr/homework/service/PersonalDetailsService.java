package au.com.enablehr.homework.service;

import java.util.List;

import org.springframework.data.domain.Page;

import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.domain.PersonalDetailsIncident;

public interface PersonalDetailsService {

    Iterable<PersonalDetails> getAll();
    
    PersonalDetails save(PersonalDetails personalDetails);

	PersonalDetails get(Integer id);
	
	Page<PersonalDetails> findPageEmployees(Integer pageNumber, String sortby);

	PersonalDetailsIncident saveIncident(PersonalDetailsIncident incident);

	List<PersonalDetailsIncident> getAllNewPersonalDetailsIncidents();

	PersonalDetails approveIncdient(Integer id);

	PersonalDetails rejectIncdient(Integer id);
	
	List<PersonalDetailsIncident> getPersonalDetailsIncidents(PersonalDetails personalDetails);

}
