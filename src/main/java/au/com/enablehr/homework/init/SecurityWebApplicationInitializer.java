package au.com.enablehr.homework.init;

import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.servlet.support.AbstractDispatcherServletInitializer;

import javax.servlet.ServletContext;

@Order(2)
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

    @Override
    protected boolean enableHttpSessionEventPublisher() {

        return true;
    }

    @Override
    protected String getDispatcherWebApplicationContextSuffix() {

        return AbstractDispatcherServletInitializer.DEFAULT_SERVLET_NAME;
    }

    @Override
    protected void beforeSpringSecurityFilterChain(final ServletContext servletContext) {

        insertFilters(servletContext, new OpenEntityManagerInViewFilter());
    }


}
