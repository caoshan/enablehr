package au.com.enablehr.homework.init;

import au.com.enablehr.homework.config.JpaConfig;
import au.com.enablehr.homework.config.SecurityConfig;
import au.com.enablehr.homework.config.ServiceConfig;
import au.com.enablehr.homework.config.TilesConfig;
import au.com.enablehr.homework.config.WebMvcConfig;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

@Order(1)
public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {

        return new Class<?>[]{JpaConfig.class,
                              SecurityConfig.class,
                              ServiceConfig.class,
                              WebMvcConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {

        return new Class<?>[]{TilesConfig.class};
    }

    @Override
    protected String[] getServletMappings() {

        return new String[]{"/"};
    }
}
