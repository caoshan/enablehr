package au.com.enablehr.homework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@ComponentScan(basePackages = "au.com.enablehr.homework.security")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsService());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {

        return super.authenticationManagerBean();
    }

    @Override
    protected UserDetailsService userDetailsService() {

        return userDetailsService;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {

        // @formatter:off
        http.authorizeRequests()
                .antMatchers("/assets/**")
                    .permitAll()
                .antMatchers("/webjars/**")
                    .permitAll()
                .anyRequest()
                    .authenticated()
                .and()
            .csrf()
                .and()
            .formLogin()
                .loginPage("/login")
                    .permitAll()
                .and()
            .logout()
                .logoutUrl("/logout")
                    .permitAll()
                .and()
            .sessionManagement()
                .and()
            .userDetailsService(userDetailsService)
        ;
        // @formatter:on
    }

}
