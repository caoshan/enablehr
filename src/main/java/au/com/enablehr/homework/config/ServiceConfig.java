package au.com.enablehr.homework.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "au.com.enablehr.homework.service")
public class ServiceConfig {

    // Nothing to see here

}
