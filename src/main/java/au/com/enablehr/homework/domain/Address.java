package au.com.enablehr.homework.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {

	@Column(name = "country")
	private String country;

	@Column(name = "post_code")
	private String postCode;

	@Column(name = "state")
	private String state;

	@Column(name = "street")
	private String street;

	@Column(name = "suburb")
	private String suburb;

	public String getCountry() {

		return country;
	}

	public void setCountry(final String country) {

		this.country = country;
	}

	public String getPostCode() {

		return postCode;
	}

	public void setPostCode(final String postCode) {

		this.postCode = postCode;
	}

	public String getState() {

		return state;
	}

	public void setState(final String state) {

		this.state = state;
	}

	public String getStreet() {

		return street;
	}

	public void setStreet(final String street) {

		this.street = street;
	}

	public String getSuburb() {

		return suburb;
	}

	public void setSuburb(final String suburb) {

		this.suburb = suburb;
	}

}
