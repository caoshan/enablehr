package au.com.enablehr.homework.domain;

public enum Status {
	INITIAL, APPROVED, REJECTED
}
