package au.com.enablehr.homework.domain;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "personal_details_incident")
public class PersonalDetailsIncident {

	@Embedded
	private Address address;

	@Column(name = "email")
	@Email
	@NotEmpty
	private String email;

	@Column(name = "family_name")
	@NotEmpty
	private String familyName;

	@Column(name = "given_name")
	@NotEmpty
	private String givenName;

	@GeneratedValue
	@Id
	private Integer id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "pd_id")
	private PersonalDetails personalDetails;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	@NotNull
	private Status status;

	public Address getAddress() {

		return address;
	}

	public void setAddress(final Address address) {

		this.address = address;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(final String email) {

		this.email = email;
	}

	public String getFamilyName() {

		return familyName;
	}

	public void setFamilyName(final String familyName) {

		this.familyName = familyName;
	}

	public String getGivenName() {

		return givenName;
	}

	public void setGivenName(final String givenName) {

		this.givenName = givenName;
	}

	public Integer getId() {

		return id;
	}

	public void setId(final Integer id) {

		this.id = id;
	}

	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
