package au.com.enablehr.homework.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "personal_details")
public class PersonalDetails {

	@Embedded
	private Address address;

	@Column(name = "email")
	@Email
	@NotEmpty
	private String email;

	@Column(name = "family_name")
	@NotEmpty
	private String familyName;

	@Column(name = "given_name")
	@NotEmpty
	private String givenName;

	@GeneratedValue
	@Id
	private Integer id;

	@OneToOne(optional = false, orphanRemoval = true)
	@JoinColumn(name = "user_id")
	private User user;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "personalDetails")
	public List<PersonalDetailsIncident> personalDetailsIncidents;

	public Address getAddress() {

		return address;
	}

	public void setAddress(final Address address) {

		this.address = address;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(final String email) {

		this.email = email;
	}

	public String getFamilyName() {

		return familyName;
	}

	public void setFamilyName(final String familyName) {

		this.familyName = familyName;
	}

	public String getGivenName() {

		return givenName;
	}

	public void setGivenName(final String givenName) {

		this.givenName = givenName;
	}

	public Integer getId() {

		return id;
	}

	public void setId(final Integer id) {

		this.id = id;
	}

	public User getUser() {

		return user;
	}

	public void setUser(final User user) {

		this.user = user;
	}

	public List<PersonalDetailsIncident> getPersonalDetailsIncidents() {
		return personalDetailsIncidents;
	}

	public void setPersonalDetailsIncidents(List<PersonalDetailsIncident> personalDetailsIncidents) {
		this.personalDetailsIncidents = personalDetailsIncidents;
	}

}
