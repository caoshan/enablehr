package au.com.enablehr.homework.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "users")
public class User implements UserDetails {

	@CollectionTable(name = "user_authority_names", joinColumns = @JoinColumn(name = "user_id"))
	@Column(name = "authority")
	@ElementCollection
	private Collection<String> authorityNames;

	@Column(name = "id")
	@GeneratedValue
	@Id
	private Integer id;

	@Column(name = "password")
	@NotEmpty
	private String password;

	@OneToOne(mappedBy = "user", optional = false, orphanRemoval = true)
	private PersonalDetails personalDetails;

	@Column(name = "username")
	@NotEmpty
	private String username;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		final ArrayList<GrantedAuthority> authorities = new ArrayList<>(authorityNames.size());

		for (final String authorityName : authorityNames) {
			authorities.add(new SimpleGrantedAuthority(authorityName));
		}

		return authorities;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(final String password) {

		this.password = password;
	}

	public String getUsername() {

		return username;
	}

	@Override
	public boolean isAccountNonExpired() {

		return true;
	}

	@Override
	public boolean isAccountNonLocked() {

		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {

		return true;
	}

	@Override
	public boolean isEnabled() {

		return true;
	}

	public void setUsername(final String username) {

		this.username = username;
	}

	public Collection<String> getAuthorityNames() {

		return authorityNames;
	}

	public void setAuthorityNames(final Collection<String> authorityNames) {

		this.authorityNames = authorityNames;
	}

	public Integer getId() {

		return id;
	}

	public void setId(final Integer id) {

		this.id = id;
	}

	public PersonalDetails getPersonalDetails() {

		return personalDetails;
	}

	public void setPersonalDetails(final PersonalDetails personalDetails) {

		this.personalDetails = personalDetails;
	}

}
