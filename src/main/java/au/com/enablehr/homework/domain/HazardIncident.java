package au.com.enablehr.homework.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;

import au.com.enablehr.homework.domain.Hazard.Severity;

@Entity
@Table(name = "hazards_incident")
public class HazardIncident {

	@Column(name = "description")
	@NotEmpty
	private String description;

	@GeneratedValue
	@Id
	private Integer id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "reported_by", nullable = false)
	@NotNull
	private User reportedBy;

	@Column(name = "reported_on")
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	@Past
	private Date reportedOn;

	@Column(name = "severity")
	@Enumerated(EnumType.STRING)
	@NotNull
	private Severity severity;

	@ManyToOne(optional = false)
	@JoinColumn(name = "hazard_id")
	private Hazard hazard;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	@NotNull
	private Status status;

	public String getDescription() {

		return description;
	}

	public void setDescription(final String description) {

		this.description = description;
	}

	public Integer getId() {

		return id;
	}

	public void setId(final Integer id) {

		this.id = id;
	}

	public User getReportedBy() {

		return reportedBy;
	}

	public void setReportedBy(final User reportedBy) {

		this.reportedBy = reportedBy;
	}

	public Date getReportedOn() {

		return reportedOn;
	}

	public void setReportedOn(final Date reportedOn) {

		this.reportedOn = reportedOn;
	}

	public Severity getSeverity() {

		return severity;
	}

	public void setSeverity(final Severity severity) {

		this.severity = severity;
	}

	public Hazard getHazard() {
		return hazard;
	}

	public void setHazard(Hazard hazard) {
		this.hazard = hazard;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
