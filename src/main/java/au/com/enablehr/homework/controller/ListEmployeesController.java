package au.com.enablehr.homework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.service.PersonalDetailsService;

@Controller
@PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')")
public class ListEmployeesController {

	private final PersonalDetailsService personalDetailsService;

	@Autowired
	public ListEmployeesController(final PersonalDetailsService personalDetailsService) {

		this.personalDetailsService = personalDetailsService;
	}

	// @ModelAttribute("employees")
	// public Iterable<PersonalDetails> employees() {
	//
	// return personalDetailsService.getAll();
	// }

	@RequestMapping("/list-employees")
	public String view() {

		return "forward:/list-employees/1";
	}

	@RequestMapping(value = "/list-employees/{pageNumber}", method = RequestMethod.GET)
	public String getEmployeePage(@PathVariable Integer pageNumber, @ModelAttribute(value = "sortby") String sortby,
			Model model) {
		
		Page<PersonalDetails> page = personalDetailsService.findPageEmployees(pageNumber, sortby);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());

		model.addAttribute("pageEmployees", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "/full/list-employees";
	}

}
