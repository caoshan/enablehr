package au.com.enablehr.homework.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.domain.HazardIncident;
import au.com.enablehr.homework.domain.Status;
import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.service.HazardService;

@Controller
@PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')")
public class ReportHazardController {

	private final HazardService hazardService;

	@Autowired
	public ReportHazardController(final HazardService hazardService) {

		this.hazardService = hazardService;
	}

	@ModelAttribute("hazard")
	public Hazard hazard(final Authentication authentication) {

		final Hazard hazard = new Hazard();

		hazard.setReportedBy((User) authentication.getPrincipal());
		hazard.setReportedOn(new Date());

		return hazard;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/report-hazard")
	public String create(@Valid final Hazard hazard, final Errors errors, final RedirectAttributes redirectAttributes) {

		if (errors.hasErrors()) {
			return "/full/report-hazard";
		}

		hazardService.save(hazard);

		redirectAttributes.addFlashAttribute("message", "Your hazard has been reported");
		return "redirect:/list-hazards";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/report-hazard")
	public String view() {

		return "/full/report-hazard";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/report-hazard/{id}")
	public String show(@PathVariable @NumberFormat Integer id, Model model) {

		Hazard hazard = hazardService.get(id);

		model.addAttribute("hazard", hazard);

		return "/full/report-hazard";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/report-hazard/{id}")
	public String update(@PathVariable @NumberFormat Integer id, @ModelAttribute Hazard hazard,
			HttpServletRequest request, Model model) {

		if (request.isUserInRole("ROLE_MANAGER")) {
			Hazard prehazard = hazardService.get(id);
			prehazard.setDescription(hazard.getDescription());
			prehazard.setSeverity(hazard.getSeverity());
			// add new attributes: lastUpdatedTime and lastUpdatedUser ??
			// hazard.setReportedOn(newHazard.getReportedOn());
			hazardService.save(prehazard);

			model.addAttribute("message", "Hazard has been updated.");
		} else {
			HazardIncident hazardIncident = new HazardIncident();
			hazardIncident.setStatus(Status.INITIAL);
			hazardIncident.setDescription(hazard.getDescription());
			hazardIncident.setSeverity(hazard.getSeverity());
			hazardIncident.setHazard(hazard);
			hazardIncident.setReportedBy(hazard.getReportedBy());
			hazardIncident.setReportedOn(new Date());
			hazardService.saveIncident(hazardIncident);
			hazard = hazardService.get(id); // reload hazard to fetch incidents
											// list
			model.addAttribute("hazard", hazard);
			model.addAttribute("message", "Hazard change request has been issued.");
		}

		// keep still in edit page or return to list page??
		return "/full/report-hazard";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/report-hazard/approve/{id}")
	public String approveIncdient(@PathVariable @NumberFormat Integer id, Model model) {

		Hazard hazard = hazardService.approveIncdient(id);

		model.addAttribute("hazard", hazard);
		model.addAttribute("message", "Hazard change has been aprroved.");

		return "/full/report-hazard";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/report-hazard/reject/{id}")
	public String rejectIncdient(@PathVariable @NumberFormat Integer id, Model model) {

		Hazard hazard = hazardService.rejectIncdient(id);

		model.addAttribute("hazard", hazard);
		model.addAttribute("message", "Hazard change is rejected.");

		return "/full/report-hazard";
	}

}
