package au.com.enablehr.homework.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.domain.PersonalDetailsIncident;
import au.com.enablehr.homework.domain.Status;
import au.com.enablehr.homework.domain.User;
import au.com.enablehr.homework.service.PersonalDetailsService;
import au.com.enablehr.homework.service.UserService;

@Controller
@PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')")
public class PersonalDetailsController {

	private final UserService userService;

	private final PersonalDetailsService personDetailsService;

	@Autowired
	public PersonalDetailsController(final UserService userService, final PersonalDetailsService personDetailsService) {

		this.userService = userService;
		this.personDetailsService = personDetailsService;
	}

	@ModelAttribute("personalDetails")
	public PersonalDetails personalDetails() {
		PersonalDetails pd = userService.getCurrentUser().getPersonalDetails();
		pd.setPersonalDetailsIncidents(personDetailsService.getPersonalDetailsIncidents(pd));											
		return pd;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/personal-details")
	public String update(@Valid final PersonalDetails personalDetails, final Errors errors,
			final RedirectAttributes redirectAttributes, HttpServletRequest request) {

		if (errors.hasErrors()) {
			return "/full/personal-details";
		}

		final User user = userService.getCurrentUser();
		if (request.isUserInRole("ROLE_MANAGER")) {

			user.setPersonalDetails(personalDetails);
			userService.save(user);

			redirectAttributes.addFlashAttribute("message", "Your details have been saved.");
		} else {
			PersonalDetailsIncident personalDetailsIncident = new PersonalDetailsIncident();
			personalDetailsIncident.setStatus(Status.INITIAL);
			personalDetailsIncident.setPersonalDetails(personalDetails);
			personalDetailsIncident.setAddress(personalDetails.getAddress());
			personalDetailsIncident.setEmail(personalDetails.getEmail());
			personalDetailsIncident.setFamilyName(personalDetails.getFamilyName());
			personalDetailsIncident.setGivenName(personalDetails.getGivenName());
			
			personDetailsService.saveIncident(personalDetailsIncident);
			redirectAttributes.addFlashAttribute("message", "Your details change request has been issued.");
		}

		return "redirect:/personal-details";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/personal-details")
	public String view() {

		return "/full/personal-details";
	}

}
