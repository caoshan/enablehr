package au.com.enablehr.homework.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.service.HazardService;

@Controller
@PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')")
public class ListHazardsController {

	private final HazardService hazardService;

	@Autowired
	public ListHazardsController(final HazardService hazardService) {

		this.hazardService = hazardService;
	}

	@ModelAttribute("myHazards")
	public List<Hazard> myHazards() {

		return hazardService.getAllForCurrentUser();
	}

	// @ModelAttribute("otherHazards")
	// public Iterable<Hazard> otherHazards() {
	//
	// return hazardService.getAll();
	// }

	@RequestMapping("/list-hazards")
	public String view() {

		return "forward: /list-hazards/1";
	}

	@RequestMapping(value = "/list-hazards/{pageNumber}", method = RequestMethod.GET)
	public String getEmployeePage(@PathVariable Integer pageNumber, @ModelAttribute(value = "sortby") String sortby,
			Model model) {
		Page<Hazard> page = hazardService.findPageHazards(pageNumber, sortby);

		int current = page.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, page.getTotalPages());

		model.addAttribute("pageHazards", page);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);

		return "/full/list-hazards";
	}

}
