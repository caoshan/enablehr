package au.com.enablehr.homework.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.domain.PersonalDetailsIncident;
import au.com.enablehr.homework.domain.Status;
import au.com.enablehr.homework.service.PersonalDetailsService;

@Controller
@PreAuthorize("hasRole('ROLE_MANAGER')")
public class EmployeesController {

	private final PersonalDetailsService personDetailsService;

	@Autowired
	public EmployeesController(final PersonalDetailsService personDetailsService) {

		this.personDetailsService = personDetailsService;
	}

	@RequestMapping("/employees")
	public String view() {

		return "/full/employees";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/employees/{id}")
	public String show(@PathVariable @NumberFormat Integer id, Model model) {

		PersonalDetails personalDetails = personDetailsService.get(id);
		model.addAttribute("personalDetails", personalDetails);

		return "/full/personal-details";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/employees/{id}")
	public String update(@PathVariable @NumberFormat Integer id, @ModelAttribute PersonalDetails newPD,
			HttpServletRequest request, final RedirectAttributes redirectAttributes) {
		PersonalDetails personalDetails = personDetailsService.get(id);
		if (request.isUserInRole("ROLE_MANAGER")) {

			personalDetails.setAddress(newPD.getAddress());
			personalDetails.setEmail(newPD.getEmail());
			personalDetails.setFamilyName(newPD.getFamilyName());
			personalDetails.setGivenName(newPD.getGivenName());
			personDetailsService.save(personalDetails);

			redirectAttributes.addFlashAttribute("message", "Your employee info has been updated");
		} else {
			PersonalDetailsIncident personalDetailsIncident = new PersonalDetailsIncident();
			personalDetailsIncident.setStatus(Status.INITIAL);
			personalDetailsIncident.setPersonalDetails(personalDetails);
			personalDetailsIncident.setAddress(newPD.getAddress());
			personalDetailsIncident.setEmail(newPD.getEmail());
			personalDetailsIncident.setFamilyName(newPD.getFamilyName());
			personalDetailsIncident.setGivenName(newPD.getGivenName());
			personDetailsService.save(personalDetails);
		}
		return "redirect:/list-employees";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/employees/approve/{id}")
	public String approveIncdient(@PathVariable @NumberFormat Integer id, Model model) {

		PersonalDetails personalDetails = personDetailsService.approveIncdient(id);
		model.addAttribute("personalDetails", personalDetails);
		model.addAttribute("message", "Personal details change has been aprroved.");

		return "/full/personal-details";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/employees/reject/{id}")
	public String rejectIncdient(@PathVariable @NumberFormat Integer id, Model model) {

		PersonalDetails personalDetails = personDetailsService.rejectIncdient(id);
		model.addAttribute("personalDetails", personalDetails);
		model.addAttribute("message", "Personal details change is ignored.");

		return "/full/personal-details";
	}

}
