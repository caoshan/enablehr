package au.com.enablehr.homework.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.enablehr.homework.domain.HazardIncident;
import au.com.enablehr.homework.domain.PersonalDetailsIncident;
import au.com.enablehr.homework.service.HazardService;
import au.com.enablehr.homework.service.PersonalDetailsService;

@Controller
public class HomeController {

	private final PersonalDetailsService personalDetailsService;

	private final HazardService hazardService;

	@Autowired
	public HomeController(final PersonalDetailsService personalDetailsService, final HazardService hazardService) {

		this.personalDetailsService = personalDetailsService;
		this.hazardService = hazardService;
	}

	@RequestMapping("/")
	public String home() {

		return "/full/home";
	}

	@ModelAttribute("personalDetailsIncidents")
	public List<PersonalDetailsIncident> getAllNewPersonalDetailsIncidents() {

		return personalDetailsService.getAllNewPersonalDetailsIncidents();
	}

	@ModelAttribute("hazardIncidents")
	public List<HazardIncident> getAllNewHazardIncidents() {

		return hazardService.getAllNewHazardIncidents();
	}

}
