package au.com.enablehr.homework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import au.com.enablehr.homework.domain.Hazard;
import au.com.enablehr.homework.domain.User;

public interface HazardRepository extends PagingAndSortingRepository<Hazard, Integer> {

	@Query("SELECT h FROM Hazard h WHERE h.reportedBy = :user")
	List<Hazard> findAllReportedBy(@Param("user") User user);

}
