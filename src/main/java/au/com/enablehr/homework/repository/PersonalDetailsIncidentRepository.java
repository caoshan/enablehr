package au.com.enablehr.homework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import au.com.enablehr.homework.domain.PersonalDetails;
import au.com.enablehr.homework.domain.PersonalDetailsIncident;

public interface PersonalDetailsIncidentRepository extends PagingAndSortingRepository<PersonalDetailsIncident, Integer> {

	@Query("SELECT p FROM PersonalDetailsIncident p WHERE p.status = 'INITIAL'")
	List<PersonalDetailsIncident> getAllNewPersonalDetailsIncidents();

	@Query("SELECT p FROM PersonalDetailsIncident p WHERE p.personalDetails = :personalDetails")
	List<PersonalDetailsIncident> getIncidents(@Param("personalDetails")PersonalDetails personalDetails);

}
