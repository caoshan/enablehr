package au.com.enablehr.homework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import au.com.enablehr.homework.domain.HazardIncident;

public interface HazardIncidentRepository extends PagingAndSortingRepository<HazardIncident, Integer> {

	@Query("SELECT h FROM HazardIncident h WHERE h.status = 'INITIAL'")
	List<HazardIncident> getAllNewHazardIncidents();

}
