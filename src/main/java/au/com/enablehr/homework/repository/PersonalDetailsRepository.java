package au.com.enablehr.homework.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import au.com.enablehr.homework.domain.PersonalDetails;

public interface PersonalDetailsRepository extends PagingAndSortingRepository<PersonalDetails, Integer> {

}
