INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    1,
    1,
    'Doe',
    'Jane',
    'jane.doe@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    2,
    2,
    'Doe',
    'John',
    'john.doe@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    3,
    3,
    'Doe',
    'Jim',
    'jim.doe@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    4,
    4,
    'Doe',
    'James',
    'james.doe@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    5,
    5,
    'Smith',
    'Jane',
    'jane.smith@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    6,
    6,
    'Smith',
    'John',
    'john.smith@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    7,
    7,
    'Smith',
    'Jim',
    'jim.smith@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    8,
    8,
    'Smith',
    'James',
    'james.smith@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    9,
    9,
    'Jones',
    'Jane',
    'jane.jones@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    10,
    10,
    'Jones',
    'John',
    'john.jones@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    11,
    11,
    'Jones',
    'Jim',
    'jim.jones@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    12,
    12,
    'Jones',
    'James',
    'james.jones@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    13,
    13,
    'Jackson',
    'Jane',
    'jane.jackson@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    14,
    14,
    'Jackson',
    'John',
    'john.jackson@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    15,
    15,
    'Jackson',
    'Jim',
    'jim.jackson@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    16,
    16,
    'Jackson',
    'James',
    'james.jackson@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    17,
    17,
    'James',
    'Jane',
    'jane.james@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    18,
    18,
    'James',
    'John',
    'john.james@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    19,
    19,
    'James',
    'Jim',
    'jim.james@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

INSERT INTO personal_details (id, user_id, family_name, given_name, email, street, suburb, state, post_code, country)
VALUES
  (
    20,
    20,
    'James',
    'James',
    'james.james@example.com',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL)
;

