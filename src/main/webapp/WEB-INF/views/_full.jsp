<%@ page contentType="text/html; UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <security:csrfMetaTags />
        <title><tiles:insertAttribute name="title" /></title>

        <tiles:importAttribute name="headScripts" />
        <c:forEach var="headScript" items="${headScripts}">
            <tags:script src="${headScript}" type="text/javascript" />
        </c:forEach>

        <tiles:importAttribute name="stylesheets" />
        <c:forEach var="stylesheet" items="${stylesheets}">
            <tags:link href="${stylesheet}" rel="stylesheet" />
        </c:forEach>

    </head>
    <body>

        <tiles:insertAttribute name="navigation" />

        <c:if test="${not empty message}">
            <div class="row">
                <div class="small-12 columns">
                    <div id="message" class="alert-box">${message}</div>
                </div>
            </div>
        </c:if>

        <section role="main">
            <tiles:insertAttribute name="content" />
        </section>

        <tiles:importAttribute name="bodyScripts" />
        <c:forEach var="bodyScript" items="${bodyScripts}">
            <tags:script src="${bodyScript}" type="text/javascript" />
        </c:forEach>
    </body>

</html>
