<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="row">
	<div class="small-12 columns">
		<h2 class="subheader">Report a hazard</h2>
	</div>
</div>

<form:form method="post" modelAttribute="hazard">

	<div class="row">
		<div class="small-12 columns">
			<spring:bind path="severity">
				<label class="${status.error ? 'error' : ''}"> Severity: <spring:eval
						expression="T(au.com.enablehr.homework.domain.Hazard$Severity).values()"
						var="severities" /> <form:select id="severity" path="severity"
						cssErrorClass="error">
						<form:option value="" cssErrorClass="error" />
						<form:options items="${severities}" cssErrorClass="error" />
					</form:select>
				</label>
				<c:if test="${status.error}">
					<small class="error">${status.errorMessage}</small>
				</c:if>
			</spring:bind>
		</div>
	</div>

	<div class="row">
		<div class="small-12 columns">
			<spring:bind path="description">
				<label class="${status.error ? 'error' : ''}"> Description:
					<form:textarea rows="10" id="description" path="description"
						type="text" cssErrorClass="error" />
				</label>
				<c:if test="${status.error}">
					<small class="error">${status.errorMessage}</small>
				</c:if>
			</spring:bind>
		</div>
	</div>

	<div class="row">
		<div class="small-12 columns">
			<hr />
		</div>
	</div>

	<div class="row">
		<div class="small-6 columns">
			<input type="reset" value="Reset"
				class="small expand secondary button" />
		</div>
		<div class="small-6 columns">
			<input type="submit" id="save" value="Save"
				class="small expand button" />
		</div>
	</div>

	<c:if test ="${not empty hazard.hazardIncidents}">
	
	<div class="row">
        <div class="small-12 columns">
            <h2 class="subheader">
                Hazard change request
            </h2>
        </div>
    </div>
	<div class="row">
		<div class="small-12 columns">

				<table id="incidents">
					<thead>
						<tr>
							<th style="width: 10em;" >Status</th>
							<th>Description</th>
							<th style="width: 5em;">Severity</th>
							<security:authorize access="hasRole('ROLE_MANAGER')">
								<th colspan="2" style="width: 12em;"></th>
							</security:authorize>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="hazardIncident" items="${hazard.hazardIncidents}">
							<tr>
								<td>${hazardIncident.status}</td>
								<td>${hazardIncident.description}</td>
								<td><c:choose>
										<c:when test="${hazardIncident.severity == 'HIGH'}">
											<span class="alert label">${hazardIncident.severity}</span>
										</c:when>
										<c:when test="${hazardIncident.severity == 'MEDIUM'}">
											<span class="secondary label">${hazardIncident.severity}</span>
										</c:when>
										<c:when test="${hazardIncident.severity == 'LOW'}">
											<span class="success label">${hazardIncident.severity}</span>
										</c:when>
										<c:otherwise>
											<span class="label">${hazardIncident.severity}</span>
										</c:otherwise>
									</c:choose></td>
									
								  <security:authorize access="hasRole('ROLE_MANAGER')">
								  <c:if test="${hazardIncident.status=='INITIAL'}">
								  <td>
										<tags:a href="/report-hazard/approve/${hazardIncident.id}" class="small radius secondary expand button">Approve</tags:a>
								   </td>
								   <td>
								   
										<tags:a href="/report-hazard/reject/${hazardIncident.id}" class="small radius secondary expand button">Reject</tags:a>
                                  </td>
                                  </c:if>
								  </security:authorize>

							</tr>
						</c:forEach>
					</tbody>
				</table>
			
		</div>
	</div>
	</c:if>
</form:form>

