<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<div class="row">
    <div class="small-12 columns">
        <h2 class="subheader">
            Your employees
        </h2>
    </div>
</div>

<div class="row">
	<div class="small-12 columns">
		<c:set var="targetUrl" value="list-employees" />
		<c:set var="totalPages" value="${pageEmployees.totalPages}" />
		<div class="pagination">
			<c:url var="firstUrl" value="/${targetUrl}/1" />
			<c:url var="lastUrl" value="/${targetUrl}/${totalPages}" />
			<c:url var="prevUrl" value="/${targetUrl}/${currentIndex - 1}" />
			<c:url var="nextUrl" value="/${targetUrl}/${currentIndex + 1}" />
			<ul>
				<c:choose>
					<c:when test="${currentIndex == 1}">
						<li class="disabled"><a href="#">&lt;&lt;</a></li>
						<li class="disabled"><a href="#">&lt;</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="${firstUrl}">&lt;&lt;</a></li>
						<li><a href="${prevUrl}">&lt;</a></li>
					</c:otherwise>
				</c:choose>
				<c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
					<c:url var="pageUrl" value="/${targetUrl}/${i}" />
					<c:choose>
						<c:when test="${i == currentIndex}">
							<li class="active"><a href="${pageUrl}"><c:out
										value="${i}" /></a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${currentIndex == totalPages}">
						<li class="disabled"><a href="#">&gt;</a></li>
						<li class="disabled"><a href="#">&gt;&gt;</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="${nextUrl}">&gt;</a></li>
						<li><a href="${lastUrl}">&gt;&gt;</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
</div>
<div class="row">
    <div class="small-12 columns">
        <table id= "employeeTb">
            <thead>
                <tr>
                    <th style="width: 15em;"><tags:a href="/${targetUrl}/${currentIndex}?sortby=givenName">Given name</tags:a></th>
                    <th style="width: 15em;"><tags:a href="/${targetUrl}/${currentIndex}?sortby=familyName">Family name</tags:a></th>
                    <th><tags:a href="/${targetUrl}/${currentIndex}?sortby=email">Email</tags:a></th>
                    <th style="width: 10em;"><tags:a href="/${targetUrl}/${currentIndex}?sortby=address">Has address?</tags:a></th>
                    <th style="width: 5em;"></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="employee" items="${pageEmployees.content}">
                    <tr>
                        <td>${employee.givenName}</td>
                        <td>${employee.familyName}</td>
                        <td><a href="mailto:${employee.email}">${employee.email}</a></td>
                        <td>
                            <c:choose>
                                <c:when test="${not empty employee.address}">
                                    <c:set var="address">
                                        <c:if test="${not empty employee.address.street}">
                                            ${employee.address.street}
                                            <br />
                                        </c:if>
                                        <c:if test="${not empty employee.address.suburb}">
                                            ${employee.address.suburb}
                                            <br />
                                        </c:if>
                                        <c:if test="${not empty employee.address.postCode}">
                                            ${employee.address.postCode}
                                        </c:if>
                                        <c:if test="${not empty employee.address.postCode
                                                      and
                                                      not empty employee.address.state}">
                                            ,
                                        </c:if>
                                        <c:if test="${not empty employee.address.state}">
                                            ${employee.address.state}
                                        </c:if>
                                        <c:if test="${(not empty employee.address.postCode
                                                       or
                                                       not empty employee.address.state)
                                                       and
                                                       not empty employee.address.country}">
                                            ,
                                        </c:if>
                                        <c:if test="${not empty employee.address.country}">
                                            ${employee.address.country}
                                        </c:if>
                                    </c:set>
                                    <span data-tooltip class="has-tip" title="${address}">
                                        <span class="label">Address</span>
                                    </span>
                                </c:when>
                                <c:otherwise>
                                    <span class="alert label">No address</span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                        	<tags:a href="/employees/${employee.id}" class="small radius secondary expand button">Edit</tags:a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script>
$(function(){
	  $('#employeeTb').tablesorter(); 
	});
</script>