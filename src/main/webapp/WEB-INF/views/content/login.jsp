<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<tags:form action="/login" method="post">

    <c:if test="${param['error'] != null}">
        <div class="row">
            <div class="small-6 small-centered columns">
                <div class="alert-box warning radius" data-alert>
                    Your login was invalid (${SPRING_SECURITY_LAST_EXCEPTION.message}), please try again.
                    <a href="#" class="close">&times;</a>
                </div>
            </div>
        </div>
    </c:if>

    <c:if test="${param['logout'] != null}">
        <div class="row">
            <div class="small-6 small-centered columns">
                <div class="alert-box success radius" data-alert>
                    You have been logged out
                    <a href="#" class="close">&times;</a>
                </div>
            </div>
        </div>
    </c:if>

    <security:csrfInput />

    <div class="row">
        <div class="small-6 small-centered columns">
            <h2 class="subheader">
                <small>Log in</small>
            </h2>
        </div>
    </div>

    <div class="row">
        <div class="small-6 small-centered columns">
            <label for="username">Username:</label>
        </div>
        <div class="small-6 small-centered columns">
            <input type="text" id="username" name="username" placeholder="email@example.com" value="jane.doe" />
        </div>
    </div>

    <div class="row">
        <div class="small-6 small-centered columns">
            <label for="password">Password</label>
        </div>
        <div class="small-6 small-centered columns">
            <input type="password" id="password" name="password" placeholder="secret" value="jane.doe"  />
        </div>
    </div>

    <div class="row">
        <div class="small-6 small-centered columns">
            <input type="submit" id="login" value="Log in" class="small expand button" />
        </div>
    </div>

</tags:form>
