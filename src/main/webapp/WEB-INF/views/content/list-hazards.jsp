<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>

<div class="row">
	<div class="small-12 columns">
		<h2 class="subheader">Your reported hazards</h2>
	</div>
</div>

<div class="row">
	<div class="small-12 columns">
		<c:choose>
			<c:when test="${empty myHazards}">
				<p>You have not reported any hazards</p>
			</c:when>
			<c:otherwise>
				<table id="myHazards">
					<thead>
						<tr>
							<th style="width: 15em;">Reported on</th>
							<th>Description</th>
							<th style="width: 5em;">Severity</th>
							<th style="width: 5em;"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="hazard" items="${myHazards}">
							<tr>
								<td><fmt:formatDate value="${hazard.reportedOn}"
										type="both" /></td>
								<td>${hazard.description}</td>
								<td><c:choose>
										<c:when test="${hazard.severity == 'HIGH'}">
											<span class="alert label">${hazard.severity}</span>
										</c:when>
										<c:when test="${hazard.severity == 'MEDIUM'}">
											<span class="secondary label">${hazard.severity}</span>
										</c:when>
										<c:when test="${hazard.severity == 'LOW'}">
											<span class="success label">${hazard.severity}</span>
										</c:when>
										<c:otherwise>
											<span class="label">${hazard.severity}</span>
										</c:otherwise>
									</c:choose></td>
								<td><tags:a href="/report-hazard/${hazard.id}"
										class="small radius secondary expand button">Edit</tags:a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<div class="row">
	<div class="small-4 push-8 columns">
		<tags:a href="/report-hazard" class="small expand button">Report a hazard</tags:a>
	</div>
</div>

<security:authorize access="hasRole('ROLE_MANAGER')">
	<div class="row">
		<div class="small-12 columns">
			<h2 class="subheader">Other reported hazards</h2>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<c:set var="targetUrl" value="list-hazards" />
			<c:set var="totalPages" value="${pageHazards.totalPages}" />
			<div class="pagination">
				<c:url var="firstUrl" value="/${targetUrl}/1" />
				<c:url var="lastUrl" value="/${targetUrl}/${totalPages}" />
				<c:url var="prevUrl" value="/${targetUrl}/${currentIndex - 1}" />
				<c:url var="nextUrl" value="/${targetUrl}/${currentIndex + 1}" />
				<ul>
					<c:choose>
						<c:when test="${currentIndex == 1}">
							<li class="disabled"><a href="#">&lt;&lt;</a></li>
							<li class="disabled"><a href="#">&lt;</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${firstUrl}">&lt;&lt;</a></li>
							<li><a href="${prevUrl}">&lt;</a></li>
						</c:otherwise>
					</c:choose>
					<c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
						<c:url var="pageUrl" value="/${targetUrl}/${i}" />
						<c:choose>
							<c:when test="${i == currentIndex}">
								<li class="active"><a href="${pageUrl}"><c:out
											value="${i}" /></a></li>
							</c:when>
							<c:otherwise>
								<li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:choose>
						<c:when test="${currentIndex == totalPages}">
							<li class="disabled"><a href="#">&gt;</a></li>
							<li class="disabled"><a href="#">&gt;&gt;</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${nextUrl}">&gt;</a></li>
							<li><a href="${lastUrl}">&gt;&gt;</a></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<c:choose>
				<c:when test="${empty pageHazards.content}">
					<p>No other hazards have been reported</p>
				</c:when>
				<c:otherwise>
					<table id="otherHazards">
						<thead>
							<tr>

								<th style="width: 15em;"><tags:a
										href="/${targetUrl}/${currentIndex}?sortby=reportedOn">Reported on</tags:a></th>
								<th style="width: 10em;"><tags:a
										href="/${targetUrl}/${currentIndex}?sortby=reportedBy">Reported by</tags:a></th>
								<th><tags:a
										href="/${targetUrl}/${currentIndex}?sortby=description">Description</tags:a></th>
								<th style="width: 5em;"><tags:a
										href="/${targetUrl}/${currentIndex}?sortby=severity">Severity</tags:a></th>
								<th style="width: 5em;"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="hazard" items="${pageHazards.content}">
								<tr>
									<td><fmt:formatDate value="${hazard.reportedOn}"
											type="both" /></td>
									<td><tags:a href="/employees/${hazard.reportedBy.id}">${hazard.reportedBy.username}</tags:a></td>
									<td>${hazard.description}</td>
									<td><c:choose>
											<c:when test="${hazard.severity == 'HIGH'}">
												<span class="alert label">${hazard.severity}</span>
											</c:when>
											<c:when test="${hazard.severity == 'MEDIUM'}">
												<span class="secondary label">${hazard.severity}</span>
											</c:when>
											<c:when test="${hazard.severity == 'LOW'}">
												<span class="success label">${hazard.severity}</span>
											</c:when>
											<c:otherwise>
												<span class="label">${hazard.severity}</span>
											</c:otherwise>
										</c:choose></td>
									<td><tags:a href="/report-hazard/${hazard.id}"
											class="small radius secondary expand button">Edit</tags:a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</security:authorize>
