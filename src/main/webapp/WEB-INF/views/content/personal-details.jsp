<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="row">
    <div class="small-12 columns">
        <h2 class="subheader">
            View and update your personal details
        </h2>
    </div>
</div>

<form:form method="post" modelAttribute="personalDetails">

    <div class="row">
        <div class="small-12 columns">
            <spring:bind path="email">
                <label class="${status.error ? 'error' : ''}">
                    Email:
                    <form:input path="email" type="email" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
    </div>

    <div class="row">
        <div class="small-12 medium-6 columns">
            <spring:bind path="givenName">
                <label class="${status.error ? 'error' : ''}">
                    Given name:
                    <form:input path="givenName" id="givenName" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
        <div class="small-12 medium-6 columns">
            <spring:bind path="familyName">
                <label class="${status.error ? 'error' : ''}">
                    Family name:
                    <form:input path="familyName" id="familyName" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
    </div>

    <div class="row">
        <div class="small-12 medium-8 columns">
            <spring:bind path="address.street">
                <label class="${status.error ? 'error' : ''}">
                    Street:
                    <form:input path="address.street" id="street" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
        <div class="small-12 medium-4 columns">
            <spring:bind path="address.suburb">
                <label class="${status.error ? 'error' : ''}">
                    Suburb:
                    <form:input path="address.suburb" id="suburb" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
    </div>

    <div class="row">
        <div class="small-6 medium-4 columns">
            <spring:bind path="address.postCode">
                <label class="${status.error ? 'error' : ''}">
                    Post code:
                    <form:input path="address.postCode" id="postCode" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
        <div class="small-6 medium-4 columns">
            <spring:bind path="address.state">
                <label class="${status.error ? 'error' : ''}">
                    State:
                    <form:input path="address.state" id="state" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
        <div class="small-12 medium-4 columns">
            <spring:bind path="address.country">
                <label class="${status.error ? 'error' : ''}">
                    Country:
                    <form:input path="address.country" id="country" type="text" cssErrorClass="error" />
                </label>
                <c:if test="${status.error}">
                    <small class="error">${status.errorMessage}</small>
                </c:if>
            </spring:bind>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="small-6 columns">
            <input type="reset" value="Reset" class="small expand secondary button" />
        </div>
        <div class="small-6 columns">
            <input type="submit" id="save" value="Save" class="small expand button" />
        </div>
    </div>
	
	<c:if test ="${not empty personalDetails.personalDetailsIncidents}">
	
	<div class="row">
        <div class="small-12 columns">
            <h2 class="subheader">
                Personal details change request
            </h2>
        </div>
    </div>
	<div class="row">
		<div class="small-12 columns">

				<table id="incidents">
					<thead>
						<tr>
							<th style="width: 10em;" >Status</th>
							<th style="width: 12em;">Given name</th>
		                    <th style="width: 12em;">Family name</th>
		                    <th style="width: 12em;">Email</th>
		                    <th style="width: 15em;">Address</th>
		                    <security:authorize access="hasRole('ROLE_MANAGER')">
		                    <th colspan="2" style="width: 12em;"></th>
		                    </security:authorize>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="pdIncident" items="${personalDetails.personalDetailsIncidents}">
							<tr>
								<td>${pdIncident.status}</td>
								<td>${pdIncident.givenName}</td>
		                        <td>${pdIncident.familyName}</td>
		                        <td><a href="mailto:${pdIncident.email}">${pdIncident.email}</a></td>
		                        <td>
		                            <c:choose>
		                                <c:when test="${not empty pdIncident.address}">
		                                    <c:set var="address">
		                                        <c:if test="${not empty pdIncident.address.street}">
		                                            ${pdIncident.address.street}
		                                            <br />
		                                        </c:if>
		                                        <c:if test="${not empty pdIncident.address.suburb}">
		                                            ${pdIncident.address.suburb}
		                                            <br />
		                                        </c:if>
		                                        <c:if test="${not empty pdIncident.address.postCode}">
		                                            ${pdIncident.address.postCode}
		                                        </c:if>
		                                        <c:if test="${not empty pdIncident.address.postCode
		                                                      and
		                                                      not empty pdIncident.address.state}">
		                                            ,
		                                        </c:if>
		                                        <c:if test="${not empty pdIncident.address.state}">
		                                            ${pdIncident.address.state}
		                                        </c:if>
		                                        <c:if test="${(not empty pdIncident.address.postCode
		                                                       or
		                                                       not empty pdIncident.address.state)
		                                                       and
		                                                       not empty pdIncident.address.country}">
		                                            ,
		                                        </c:if>
		                                        <c:if test="${not empty pdIncident.address.country}">
		                                            ${pdIncident.address.country}
		                                        </c:if>
		                                    </c:set>
		                                    <span data-tooltip class="has-tip" title="${address}">
		                                        <span class="label">Address</span>
		                                    </span>
		                                </c:when>
		                                <c:otherwise>
		                                    <span class="alert label">No address</span>
		                                </c:otherwise>
		                            </c:choose>
		                        </td>
		                        <security:authorize access="hasRole('ROLE_MANAGER')">
		                          <c:if test="${pdIncident.status=='INITIAL'}">
								  <td>
										<tags:a href="/employees/approve/${pdIncident.id}" class="small radius secondary expand button">Approve</tags:a>
							      </td>
							      <td>
										<tags:a href="/employees/reject/${pdIncident.id}" class="small radius secondary expand button">Reject</tags:a>
                                  </td>
                                  </c:if>
								  </security:authorize>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			
		</div>
	</div>
	</c:if>
	
</form:form>

