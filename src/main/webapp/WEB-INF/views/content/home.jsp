<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>

<div class="row">
	<div class="small-8 columns">
		<h2 class="subheader">
			Welcome back
			<security:authentication property="principal.username" />
		</h2>
	</div>
</div>

<div class="row">
	<div class="small-8 columns">
		<ul>
			<security:authorize
				access="hasAnyRole('ROLE_EMPLOYEE','ROLE_MANAGER')">
				<li><tags:a href="/personal-details">
                        View and update your personal details
                    </tags:a></li>
			</security:authorize>
			<security:authorize access="hasRole('ROLE_MANAGER')">
				<li><tags:a href="/list-employees">
                        Manage your employees
                    </tags:a></li>
			</security:authorize>
			<security:authorize
				access="hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')">
				<li><tags:a href="/list-hazards">
                        Report a workplace hazard
                    </tags:a></li>
			</security:authorize>
		</ul>
	</div>
</div>

<security:authorize access="hasRole('ROLE_MANAGER')">
	<c:if test="${not empty hazardIncidents}">

		<div class="row">
			<div class="small-12 columns">
				<h2 class="subheader">Hazard change request</h2>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">

				<table id="incidents">
					<thead>
						<tr>
							<th style="width: 10em;">Status</th>
							<th>Description</th>
							<th style="width: 5em;">Severity</th>
							<th style="width: 5em;"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="hazardIncident" items="${hazardIncidents}">
							<tr>
								<td>${hazardIncident.status}</td>
								<td>${hazardIncident.description}</td>
								<td><c:choose>
										<c:when test="${hazardIncident.severity == 'HIGH'}">
											<span class="alert label">${hazardIncident.severity}</span>
										</c:when>
										<c:when test="${hazardIncident.severity == 'MEDIUM'}">
											<span class="secondary label">${hazardIncident.severity}</span>
										</c:when>
										<c:when test="${hazardIncident.severity == 'LOW'}">
											<span class="success label">${hazardIncident.severity}</span>
										</c:when>
										<c:otherwise>
											<span class="label">${hazardIncident.severity}</span>
										</c:otherwise>
									</c:choose></td>
 								<td>
                                    <tags:a href="/report-hazard/${hazardIncident.hazard.id}" class="small radius secondary expand button">Check</tags:a>
                                </td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>
		</div>
	</c:if>

	<c:if test="${not empty personalDetailsIncidents}">

		<div class="row">
			<div class="small-12 columns">
				<h2 class="subheader">Personal details change request</h2>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">

				<table id="incidents">
					<thead>
						<tr>
							<th style="width: 10em;">Status</th>
							<th style="width: 15em;">Given name</th>
							<th style="width: 15em;">Family name</th>
							<th>Email</th>
							<th style="width: 10em;">Address</th>
							<th style="width: 5em;"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="pdIncident"
							items="${personalDetailsIncidents}">
							<tr>
								<td>${pdIncident.status}</td>
								<td>${pdIncident.givenName}</td>
								<td>${pdIncident.familyName}</td>
								<td><a href="mailto:${pdIncident.email}">${pdIncident.email}</a></td>
								<td><c:choose>
										<c:when test="${not empty pdIncident.address}">
											<c:set var="address">
												<c:if test="${not empty pdIncident.address.street}">
		                                            ${pdIncident.address.street}
		                                            <br />
												</c:if>
												<c:if test="${not empty pdIncident.address.suburb}">
		                                            ${pdIncident.address.suburb}
		                                            <br />
												</c:if>
												<c:if test="${not empty pdIncident.address.postCode}">
		                                            ${pdIncident.address.postCode}
		                                        </c:if>
												<c:if
													test="${not empty pdIncident.address.postCode
		                                                      and
		                                                      not empty pdIncident.address.state}">
		                                            ,
		                                        </c:if>
												<c:if test="${not empty pdIncident.address.state}">
		                                            ${pdIncident.address.state}
		                                        </c:if>
												<c:if
													test="${(not empty pdIncident.address.postCode
		                                                       or
		                                                       not empty pdIncident.address.state)
		                                                       and
		                                                       not empty pdIncident.address.country}">
		                                            ,
		                                        </c:if>
												<c:if test="${not empty pdIncident.address.country}">
		                                            ${pdIncident.address.country}
		                                        </c:if>
											</c:set>
											<span data-tooltip class="has-tip" title="${address}">
												<span class="label">Address</span>
											</span>
										</c:when>
										<c:otherwise>
											<span class="alert label">No address</span>
										</c:otherwise>
									</c:choose></td>
									<td>
                                    	<tags:a href="/employees/${pdIncident.personalDetails.id}" class="small radius secondary expand button">Check</tags:a>
                                	</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>
		</div>
	</c:if>
</security:authorize>
