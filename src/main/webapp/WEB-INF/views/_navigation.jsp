<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<security:authorize access="isAuthenticated()">

    <nav class="top-bar" data-topbar>

        <ul class="title-area">
            <li class="name">
                <h1>
                    <tags:a href="/">
                        <tiles:insertAttribute name="title" />
                    </tags:a>
                </h1>
            </li>
            <li class="toggle-topbar menu-icon">
                <a href="#">
                    <span>Menu</span>
                </a>
            </li>
        </ul>

        <section class="top-bar-section">
            <security:authorize access="isAuthenticated()">
                <ul class="right">
                    <security:authorize access="hasAnyRole('ROLE_EMPLOYEE','ROLE_MANAGER')">
                        <li class="divider"></li>
                        <li>
                            <tags:a href="/personal-details">
                                <i class="fa fa-user"></i>
                                Personal details
                            </tags:a>
                        </li>
                    </security:authorize>
                    <security:authorize access="hasRole('ROLE_MANAGER')">
                        <li class="divider"></li>
                        <li>
                            <tags:a href="/list-employees">
                                <i class="fa fa-users"></i>
                                Employees
                            </tags:a>
                        </li>
                    </security:authorize>
                    <security:authorize access="hasAnyRole('ROLE_EMPLOYEE', 'ROLE_MANAGER')">
                        <li>
                            <tags:a href="/list-hazards">
                                <i class="fa fa-warning"></i>
                                Hazards
                            </tags:a>
                        </li>
                    </security:authorize>
                    <li class="divider"></li>
                    <li class="has-form">
                        <tags:form action="/logout" method="post">
                            <security:csrfInput />
                            <input type="submit" value="Log out" class="small button" />
                        </tags:form>
                    </li>
                </ul>
            </security:authorize>
        </section>

    </nav>


</security:authorize>
