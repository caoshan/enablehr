<%@ tag description="Rdenders a context-relative &lt;script&gt; tag" %>
<%@ tag display-name="script" %>
<%@ tag dynamic-attributes="attrs" %>
<%@ tag example="&lt;tags:script src=&quot;/hello&quot;&gt;&lt;/tags:script&gt;" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="fullAttrs" value="" />
<c:forEach var="attr" items="${attrs}">
    <c:choose>
        <c:when test="${attr.key == 'src'}">
            <c:url var="src" value="${attr.value}" />
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${src}&quot;" />
        </c:when>
        <c:otherwise>
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${attr.value}&quot;" />
        </c:otherwise>
    </c:choose>
</c:forEach>

<jsp:doBody var="body" />

<script ${fullAttrs}>${body}</script>
