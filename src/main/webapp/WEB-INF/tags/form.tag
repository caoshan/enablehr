<%@ tag description="Rdenders a context-relative &lt;form&gt; tag" %>
<%@ tag display-name="form" %>
<%@ tag dynamic-attributes="attrs" %>
<%@ tag example="&lt;tags:form action=&quot;/submit&quot;&gt;...&lt;/tags:form&gt;" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="fullAttrs" value="" />
<c:forEach var="attr" items="${attrs}">
    <c:choose>
        <c:when test="${attr.key == 'action'}">
            <c:url var="action" value="${attr.value}" />
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${action}&quot;" />
        </c:when>
        <c:otherwise>
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${attr.value}&quot;" />
        </c:otherwise>
    </c:choose>
</c:forEach>

<form ${fullAttrs}>
    <jsp:doBody />
</form>
