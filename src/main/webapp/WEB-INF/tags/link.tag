<%@ tag body-content="empty" %>
<%@ tag description="Rdenders a context-relative &lt;link&gt; tag" %>
<%@ tag display-name="link" %>
<%@ tag dynamic-attributes="attrs" %>
<%@ tag example="&lt;tags:link href=&quot;/hello.css&quot;/&gt;" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="fullAttrs" value="" />
<c:forEach var="attr" items="${attrs}">
    <c:choose>
        <c:when test="${attr.key == 'href'}">
            <c:url var="href" value="${attr.value}" />
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${href}&quot;" />
        </c:when>
        <c:otherwise>
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${attr.value}&quot;" />
        </c:otherwise>
    </c:choose>
</c:forEach>

<link ${fullAttrs} />
