<%@ tag description="Rdenders a context-relative &lt;a&gt; tag" %>
<%@ tag display-name="a" %>
<%@ tag dynamic-attributes="attrs" %>
<%@ tag example="&lt;tags:a href=&quot;/hello&quot;&gt;hello&lt;/tags:a&gt;" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="fullAttrs" value="" />
<c:forEach var="attr" items="${attrs}">
    <c:choose>
        <c:when test="${attr.key == 'href'}">
            <c:url var="href" value="${attr.value}" />
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${href}&quot;" />
        </c:when>
        <c:otherwise>
            <c:set var="fullAttrs" value="${fullAttrs} ${attr.key}=&quot;${attr.value}&quot;" />
        </c:otherwise>
    </c:choose>
</c:forEach>

<jsp:doBody var="body" />

<a ${fullAttrs}>${body}</a>
